JAVA?=java
JAVAC?=javac
JAVADOC?=javadoc
JAR?=jar
JCFLAGS?=-source 1.5 -Xlint:all
MSGFMT?=msgfmt

PREFIX?=/usr/local
JARDIR?=$(PREFIX)/share/java
DOCDIR?=$(PREFIX)/share/doc/pescetti/
MANDIR?=$(PREFIX)/share/man/man1/
SHAREDIR?=$(PREFIX)/share/pescetti/
BINDIR?=$(PREFIX)/bin/

DEBUG?=disable

CLASSPATH=classes:/usr/share/java/debug-$(DEBUG).jar:/usr/share/java/itext.jar

VERSION=$(shell sed -n '1s/^Version \([0-9.]*\):$$/\1/p' changelog)

SRC=$(shell find cx -name '*.java')

all: pescetti-$(VERSION).jar dup2dds-$(VERSION).jar bin/dup2dds pescetti.1 bin/pescetti dealing-howto.pdf

.bin:
	mkdir -p bin
	touch .bin
.testbin:
	mkdir -p testbin
	touch .testbin
classes: .classes 
.classes: $(SRC) translations/*.po
	mkdir -p classes
	$(JAVAC) $(JCFLAGS) -cp $(CLASSPATH):classes -d classes $(SRC)
	(cd translations; for i in *.po; do $(MSGFMT) --java2 -r pescetti_localized -d ../classes -l $${i%.po} $$i; done)
	$(MSGFMT) --java2 -r pescetti_localized -d classes translations/en_GB.po
	touch .classes
clean:
	rm -rf classes bin testbin pescetti-$(VERSION)
	rm -f .classes .bin .testbin *.tar.gz *.jar pescetti.1 *Manifest.txt *.dvi *.pdf *.ps *.log *.aux

pescetti-$(VERSION).jar: PescettiManifest.txt .classes
	(cd classes; $(JAR) cfm ../$@ ../$< `find cx -name '*.class' -and -not -name 'dup2dds.class'` *localized*class)
dup2dds-$(VERSION).jar: DupManifest.txt .classes
	(cd classes; $(JAR) cfm ../$@ ../$< cx/ath/matthew/pescetti/dup2dds.class)

pescetti-$(VERSION).tar.gz: Makefile cx README INSTALL COPYING changelog pescetti.sh pescetti.sgml Manifest.txt.in dealing-howto.tex translations dup2dds.sh dup2dds.1 pbn2dds pbn2dds.1
	mkdir -p pescetti-$(VERSION)
	cp -a $^ pescetti-$(VERSION)
	tar zcf $@ pescetti-$(VERSION)

itext.jar: 
	ln -sf /usr/share/java/itext.jar .
debug-$(DEBUG).jar: 
	ln -sf /usr/share/java/debug-$(DEBUG).jar .

bin/%: %.sh .bin
	sed 's,\%JARPATH\%,$(JARDIR),;s,\%VERSION\%,$(VERSION),;s,\%DEBUG\%,$(DEBUG),;s,\%JAVA\%,$(JAVA),' < $< > $@

testbin/%: %.sh .testbin dup2dds-$(VERSION).jar pescetti-$(VERSION).jar debug-$(DEBUG).jar itext.jar
	sed 's,\%JARPATH\%,.,;s,\%VERSION\%,$(VERSION),;s,\%DEBUG\%,$(DEBUG),;s,\%JAVA\%,$(JAVA),' < $< > $@
	chmod 755 $@

%.pdf: %.tex
	pdflatex $^

%.1: %.sgml
	docbook-to-man $< > $@

DupManifest.txt: Manifest.txt.in
	echo Main-Class: cx.ath.matthew.pescetti.dup2dds > $@
	echo Class-Path: $(SHAREDIR)/pescetti.jar >> $@
	cat $< >> $@
	echo "Implementation-Version: $(VERSION)" >> $@
PescettiManifest.txt: Manifest.txt.in
	echo Main-Class: cx.ath.matthew.pescetti.pescetti > $@
ifeq ($(DEBUG),enable)
	echo Class-Path: $(JARDIR)/debug-$(DEBUG).jar $(JARDIR)/itext.jar >> $@
else
	echo Class-Path: $(JARDIR)/itext.jar >> $@
endif
	cat $< >> $@
	echo "Implementation-Version: $(VERSION)" >> $@

translations/en_GB.po: $(SRC)
	echo "#java-format" > $@
	sed -n '/_(/s/.*_("\([^"]*\)").*/\1/p' $^ | sort -u | sed 's/\(.*\)/msgid "\1"\nmsgstr "\1"/' >> $@

install: pescetti.1 dup2dds.1 pbn2dds.1 bin/dup2dds bin/pescetti pescetti-$(VERSION).jar dup2dds-$(VERSION).jar changelog COPYING INSTALL README dealing-howto.pdf
	install -d $(DESTDIR)$(BINDIR)
	install bin/pescetti $(DESTDIR)$(BINDIR)
	install bin/dup2dds $(DESTDIR)$(BINDIR)
	install pbn2dds $(DESTDIR)$(BINDIR)
	install -d $(DESTDIR)$(MANDIR)
	install -m 644 pescetti.1 dup2dds.1 $(DESTDIR)$(MANDIR)
	install -d $(DESTDIR)$(SHAREDIR)
	install -m 644 pescetti-$(VERSION).jar $(DESTDIR)$(SHAREDIR)
	install -m 644 dup2dds-$(VERSION).jar $(DESTDIR)$(SHAREDIR)
	ln -sf pescetti-$(VERSION).jar $(DESTDIR)$(SHAREDIR)/pescetti.jar
	ln -sf dup2dds-$(VERSION).jar $(DESTDIR)$(SHAREDIR)/dup2dds.jar
	install -d $(DESTDIR)$(DOCDIR)
	install -m 644 changelog COPYING INSTALL README dealing-howto.pdf $(DESTDIR)$(DOCDIR)

uninstall:
	rm -f $(DESTDIR)$(BINDIR)/pescetti  $(DESTDIR)$(BINDIR)/pbn2dds
	rm -f $(DESTDIR)$(SHAREDIR)/pescetti-$(VERSION).jar $(DESTDIR)$(SHAREDIR)/pescetti.jar
	rm -f $(DESTDIR)$(SHAREDIR)/dup2dds-$(VERSION).jar $(DESTDIR)$(SHAREDIR)/dup2dds.jar
	rm -f $(DESTDIR)$(MANDIR)/pescetti.1
	rm -f $(DESTDIR)$(MANDIR)/dup2dds.1
	rm -f $(DESTDIR)$(DOCDIR)/changelog $(DESTDIR)$(DOCDIR)/COPYING $(DESTDIR)$(DOCDIR)/INSTALL $(DESTDIR)$(DOCDIR)/README $(DESTDIR)$(DOCDIR)/dealing-howto.pdf
	-rmdir $(DESTDIR)$(BINDIR) $(DESTDIR)$(MANDIR) $(DESTDIR)$(SHAREDIR) $(DESTDIR)$(DOCDIR)
