#!/bin/sh --

JAVA=%JAVA%
VERSION=%VERSION%
JARPATH=%JARPATH%

exec $JAVA -jar $JARPATH/dup2dds-$VERSION.jar "$@"
