/* 
 * Pescetti Pseudo-Duplimate Generator
 * 
 * Copyright (C) 2007 Matthew Johnson
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License Version 2 as published by
 * the Free Software Foundation.  This program is distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.  You should have received a
 * copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * To Contact me, please email src@matthew.ath.cx
 *
 */

package cx.ath.matthew.pescetti;

public interface Constants
{
   public static final byte SPADES = 0x10;
   public static final byte HEARTS = 0x20;
   public static final byte DIAMONDS = 0x30;
   public static final byte CLUBS = 0x40;
   public static final byte NOTRUMPS = 0x50;
   public static final byte TEN = 0x0A;
   public static final byte JACK = 0x0B;
   public static final byte QUEEN = 0x0C;
   public static final byte KING = 0x0D;
   public static final byte ACE = 0x0E;
   public static final byte[] INITIAL_SPADES =
      {SPADES | ACE, SPADES | KING, SPADES | QUEEN, SPADES | JACK, SPADES | TEN, SPADES | 9, SPADES | 8, SPADES | 7, SPADES | 6, SPADES | 5, SPADES | 4, SPADES | 3, SPADES | 2};
   public static final byte[] INITIAL_HEARTS =
      {HEARTS | ACE, HEARTS | KING, HEARTS | QUEEN, HEARTS | JACK, HEARTS | TEN, HEARTS | 9, HEARTS | 8, HEARTS | 7, HEARTS | 6, HEARTS | 5, HEARTS | 4, HEARTS | 3, HEARTS | 2};
   public static final byte[] INITIAL_DIAMONDS =
      {DIAMONDS | ACE, DIAMONDS | KING, DIAMONDS | QUEEN, DIAMONDS | JACK, DIAMONDS | TEN, DIAMONDS | 9, DIAMONDS | 8, DIAMONDS | 7, DIAMONDS | 6, DIAMONDS | 5, DIAMONDS | 4, DIAMONDS | 3, DIAMONDS | 2};
   public static final byte[] INITIAL_CLUBS =
      {CLUBS | ACE, CLUBS | KING, CLUBS | QUEEN, CLUBS | JACK, CLUBS | TEN, CLUBS | 9, CLUBS | 8, CLUBS | 7, CLUBS | 6, CLUBS | 5, CLUBS | 4, CLUBS | 3, CLUBS | 2};
   public static final byte[][] INITIAL_HAND = {
      INITIAL_SPADES, INITIAL_HEARTS, INITIAL_DIAMONDS, INITIAL_CLUBS };
   public static final String[] PLAYER = { "North", "East", "South", "West" };
   public static final char[] SUIT = { '!', '♠', '♡', '♢', '♣', 'N' };
   public static final char[] SHORTSUIT = { '!', 'S', 'H', 'D', 'C', 'N' };
   public static final char[] DINGSUIT = { '!', (char) 171, (char) 170, (char) 169, (char) 168, 'N' };
   public static final String[] LONGSUIT = { "!", "Spades", "Hearts", "Diamonds", "Clubs", "No Trumps" };
   public static final char[] CARD = { '!', '!',  '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A' };
   public static final int NORTH = 0;
   public static final int EAST = 1;
   public static final int SOUTH = 2;
   public static final int WEST = 3;

   public static final int STAT_NSGAMES = 0;
   public static final int STAT_EWGAMES = 1;
   public static final int STAT_NSSLAMS = 2;
   public static final int STAT_EWSLAMS = 3;
   public static final int STAT_NPOINTS = 4;
   public static final int STAT_SPOINTS = 5;
   public static final int STAT_EPOINTS = 6;
   public static final int STAT_WPOINTS = 7;
   public static final int STAT_4333 = 8;
   public static final int STAT_BAL = 9;
   public static final int STAT_6PLUS = 10;
   public static final int STAT_7PLUS = 11;
   public static final int STAT_8PLUS = 12;
   public static final int STAT_BACON = 13;
   public static final int STAT_SEMIBAL = 14;
   public static final int STAT_55 = 15;
   public static final int STAT_VOID = 16;
   public static final int STAT_4441 = 17;
   public static final int STAT_MAX = 18;

}
