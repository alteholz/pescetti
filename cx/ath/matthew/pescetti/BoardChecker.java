/* 
 * Pescetti Pseudo-Duplimate Generator
 * 
 * Copyright (C) 2007 Matthew Johnson
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License Version 2 as published by
 * the Free Software Foundation.  This program is distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.  You should have received a
 * copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * To Contact me, please email src@matthew.ath.cx
 *
 */

package cx.ath.matthew.pescetti;

import cx.ath.matthew.debug.Debug;

import static cx.ath.matthew.pescetti.pescetti.count;
import static cx.ath.matthew.pescetti.pescetti.length;
import static cx.ath.matthew.pescetti.pescetti.contains;

import java.util.HashMap;
import java.util.Random;

/* 
 * Takes a set of criteria for a board and a board
 * and checks whether it matches.
 */
public class BoardChecker
{
   private HashMap<Criteria, Double> criteria;
	private Random r;
   public BoardChecker(HashMap<Criteria, Double> criteria)
   {
      this.criteria = criteria;
		this.r = new Random();
   }
   public boolean check(byte[][] board)
   {
      if (Debug.debug) Debug.print(Debug.DEBUG, "Checking "+board
            +" against "+criteria.size()+" criteria");
		double cw = ((double) r.nextInt(1073741824)) / 1073741824.0;
      for (Criteria c: criteria.keySet()) {
         if (Debug.debug) Debug.print(Debug.DEBUG, "Checking "+board
               +" against "+c+" returns "+c.match(board));
         if (c.match(board) && criteria.get(c) > cw) {
				if (Debug.debug) Debug.print(Debug.INFO, "Board matches "+c);
				return true;
			}
      }
      return false;
   }
}

