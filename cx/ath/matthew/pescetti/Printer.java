/* 
 * Pescetti Pseudo-Duplimate Generator
 * 
 * Copyright (C) 2007 Matthew Johnson
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License Version 2 as published by
 * the Free Software Foundation.  This program is distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.  You should have received a
 * copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * To Contact me, please email src@matthew.ath.cx
 *
 */
package cx.ath.matthew.pescetti;

import java.util.Arrays;

import cx.ath.matthew.debug.Debug;

public abstract class Printer implements Constants
{
   
   public void doPlayerPrintable(String[] out, int j /* offs */, byte[] hand, char[] suits)
   {
      Arrays.sort(hand);           
      byte lastsuit = SPADES >> 4;
      int k = j;
      for (int i = 0; i < hand.length ; ) {
         while (i < hand.length && lastsuit == (hand[i] >> 4)) {
            if (Debug.debug) Debug.print(Debug.DEBUG, j+" "+(hand[i] & 0x0f));
            out[j] = CARD[hand[i] & 0x0f] + (null==out[j]?"":out[j]);
            i++;
         }
         if (i < hand.length) {
            j += (hand[i] >> 4) - lastsuit;
            lastsuit = (byte) (hand[i] >> 4);
         }
      }
      for (int i = k; i < k+4; i++)
         out[i] = suits[i-k+1]+" "+(null==out[i]?"":out[i]);
   }

   /**
    * Returns a 16 element array of North Spades, North Hearts, ... West Spades, ... East, ... South Spades, ... South Clubs.
    */
   public String[] getHandPrintables(byte[][] hand)
	{
		return getHandPrintables(hand, SUIT);
	}
   public String[] getHandPrintables(byte[][] hand, char[] suits)
   {
      String[] out = new String[16];
      doPlayerPrintable(out, 0, hand[NORTH], suits);
      doPlayerPrintable(out, 4, hand[WEST], suits);
      doPlayerPrintable(out, 8, hand[EAST], suits);
      doPlayerPrintable(out, 12, hand[SOUTH], suits);
      return out;
   }

	@SuppressWarnings("fallthrough")
   public static int count(byte[] hand)
   {
      int total = 0;
      for (byte a: hand) {
         int b = a & 0x0f;
         if (Debug.debug) Debug.print(Debug.DEBUG,"a = "+a+" b = "+b+" / "+CARD[b]);
         switch (b) {
            case ACE: total++;
            case KING: total++;
            case QUEEN: total++;
            case JACK: total++;
         }
         if (Debug.debug) Debug.print(Debug.DEBUG,"total="+total);
      }
      return total;
   }
   public abstract void printHand(int num, byte[][] hand);
   public abstract void printCurtains(int num, byte[][] hand);
   public abstract void printCurtainBack(int num);
   public abstract void printPermutation(int permnum, int boardnum, byte[] perm, byte[] initperm);
   public abstract void printPermutation1(int boardnum, byte[][] perm);
   public abstract void close();
   public abstract void printTricks(byte[] tricks);
   public abstract void printPoints(byte[] points);
   public abstract void printStats(double[] stats);
   public abstract void println(String s);
   public abstract void print(String s);
}

