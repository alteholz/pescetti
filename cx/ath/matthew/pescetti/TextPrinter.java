/* 
 * Pescetti Pseudo-Duplimate Generator
 * 
 * Copyright (C) 2007 Matthew Johnson
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License Version 2 as published by
 * the Free Software Foundation.  This program is distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.  You should have received a
 * copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * To Contact me, please email src@matthew.ath.cx
 *
 */
package cx.ath.matthew.pescetti;

import cx.ath.matthew.debug.Debug;

import java.io.PrintStream;
import java.text.DecimalFormat;

public class TextPrinter extends Printer implements Constants
{
   private PrintStream out;
   public TextPrinter(PrintStream out, String header)
   {
      this.out = out;
      out.println(header);
      for (int i = 0; i < header.length(); i++)
         out.print('-');
      out.println();
   }
   public void printHand(int num, byte[][] hand)
   {
      String[] printablehand = getHandPrintables(hand);
      out.println("Board "+num+": ");
      //   0 
      // 3   1
      //   2
      out.println("         "+PLAYER[NORTH]);
      for (int i = 0; i < 4; i++) {
         out.print("         ");
         out.println(printablehand[i]);
      }
      out.println("         HCP: "+count(hand[NORTH]));
      out.println();
      out.println(PLAYER[WEST]+"              "+PLAYER[EAST]);
      for (int i = 4; i < 8; i++) {
         out.print(printablehand[i]);
         for (int j = 0; j < (16-printablehand[i].length()); j++)
            out.print(' ');
         out.println(printablehand[i+4]);
      }
      String hcp = ""+count(hand[WEST]);
      out.print("HCP: "+hcp);
      for (int j = 0; j < (11-hcp.length()); j++)
         out.print(' ');
      out.println("HCP: "+count(hand[EAST]));
      out.println();
      out.println("          "+PLAYER[SOUTH]);
      for (int i = 12; i < 16; i++) {
         out.print("          ");
         out.println(printablehand[i]);
      }
      out.println("          HCP: "+count(hand[SOUTH]));
      out.println();
   }
   public void printCurtains(int num, byte[][] hand)
   {
      String[] printablehand = getHandPrintables(hand);
      out.println("Board "+num+" "+PLAYER[NORTH]+": ");
      for (int i = 0; i < 4; i++) {
         out.print(" ");
         out.println(printablehand[i]);
      }
      out.println();
      out.println("---------------------------------");
      out.println();
      out.println("Board "+num+" "+PLAYER[EAST]+": ");
      for (int i = 8; i < 12; i++) {
         out.print(" ");
         out.println(printablehand[i]);
      }
      out.println();
      out.println("---------------------------------");
      out.println();
      out.println("Board "+num+" "+PLAYER[SOUTH]+": ");
      for (int i = 12; i < 16; i++) {
         out.print(" ");
         out.println(printablehand[i]);
      }
      out.println();
      out.println("---------------------------------");
      out.println();
      out.println("Board "+num+" "+PLAYER[WEST]+": ");
      for (int i = 4; i < 8; i++) {
         out.print(" ");
         out.println(printablehand[i]);
      }
      out.println();
      out.println("---------------------------------");
      out.println();
   }
   public void printCurtainBack(int num)
   {
      return; // do nothing
   }
   public void printPermutation1(int boardnum, byte[][] perm)
   {
      out.println("Board "+boardnum+" permutation 1:");
      out.println("Deal this number into each pile:");
      out.println();
      for (byte i: perm[0]) {
         out.print(i);
         out.print(' ');
      }
      out.println();
      out.println();
      out.println("Pick the piles up in this order:");
      out.println();
      for (byte i: perm[1]) {
         out.print(i);
         out.print(' ');
      }
      out.println();
      out.println();
   }
   public void printPermutation(int permnum, int boardnum, byte[] perm, byte[] initperm)
   {
      out.println("Board "+boardnum+" permutation "+permnum+":");
      if (null != initperm) {
         out.print("Order the suits: ");
         for (int i = 0; i < 4; i++) {
            out.print(LONGSUIT[initperm[i] >> 4]);
            out.print(' ');
         }
         out.println();
      }
      out.print("   ");
      for (int k = 0; k < 52; k++) {
         out.print(perm[k]+1);
         if (3 == k % 4)
            out.print(' ');
         if (23 == k % 24) {
            out.println();
            out.print("   ");
         }
      }
      out.println();
      out.println();
   }

   public void close()
   {
      out.println();
      out.close();
   }

   public void printPoints(byte[] points)
	{}
   public void printTricks(byte[] tricks)
   {
      out.println("  "+SUIT[CLUBS>>4]+"  "+SUIT[DIAMONDS>>4]+"  "+SUIT[HEARTS>>4]+"  "+SUIT[SPADES>>4]+"  "+SUIT[NOTRUMPS>>4]);
      out.print("N");
      for (int i = 4; i >= 0; i--) out.print(" "+tricks[i]+(tricks[i] < 10 ? " ":""));
      out.println();
      out.print("S");
      for (int i = 9; i >= 5; i--) out.print(" "+tricks[i]+(tricks[i] < 10 ? " ":""));
      out.println();
      out.print("E");
      for (int i = 14; i >= 10; i--) out.print(" "+tricks[i]+(tricks[i] < 10 ? " ":""));
      out.println();
      out.print("W");
      for (int i = 19; i >= 15; i--) out.print(" "+tricks[i]+(tricks[i] < 10 ? " ":""));
      out.println();
      out.println();
   }
   public void println(String s)
   {
      out.println(s);
   }
   public void print(String s)
   {
      out.print(s);
   }
   public void printStats(double[] stats)
   {
      DecimalFormat df = new DecimalFormat("##.##");
      out.print("North avg points: ");
      out.println(df.format(stats[STAT_NPOINTS]));
      out.print("South avg points: ");
      out.println(df.format(stats[STAT_SPOINTS]));
      out.print("East avg points: ");
      out.println(df.format(stats[STAT_EPOINTS]));
      out.print("West avg points: ");
      out.println(df.format(stats[STAT_WPOINTS]));
      out.print("North-South Games: ");
      out.println((int)stats[STAT_NSGAMES]);
      out.print("East-West Games: ");
      out.println((int)stats[STAT_EWGAMES]);
      out.print("North-South Slams: ");
      out.println((int)stats[STAT_NSSLAMS]);
      out.print("East-West Slams: ");
      out.println((int)stats[STAT_EWSLAMS]);
      out.print("# 4333 Hands: ");
      out.println((int) stats[STAT_4333]);
      out.print("# Balanced Hands: ");
      out.println((int) stats[STAT_BAL]);
      out.print("# Semi-Balanced Hands: ");
      out.println((int) stats[STAT_SEMIBAL]);
      out.print("# 4441 Hands: ");
      out.println((int) stats[STAT_4441]);
      out.print("# 5-5 Hands: ");
      out.println((int) stats[STAT_55]);
      out.print("# 6+ Suits: ");
      out.println((int)stats[STAT_6PLUS]);
      out.print("# 7+ Suits: ");
      out.println((int)stats[STAT_7PLUS]);
      out.print("# 8+ Suits: ");
      out.println((int)stats[STAT_8PLUS]);
      out.print("# Voids: ");
      out.println((int) stats[STAT_VOID]);
      out.print("# Bacons: ");
      out.println((int) stats[STAT_BACON]);
   }

}

