<!doctype refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN" [

<!-- Process this file with docbook-to-man to generate an nroff manual
     page: `docbook-to-man manpage.sgml > manpage.1'.  You may view
     the manual page with: `docbook-to-man manpage.sgml | nroff -man |
     less'.  A typical entry in a Makefile or Makefile.am is:

manpage.1: manpage.sgml
	docbook-to-man $< > $@

    
	The docbook-to-man binary is found in the docbook-to-man package.
	Please remember that if you create the nroff version in one of the
	debian/rules file targets (such as build), you will need to include
	docbook-to-man in your Build-Depends control field.

  -->

  <!-- Fill in your name for FIRSTNAME and SURNAME. -->
  <!ENTITY dhfirstname "<firstname>Matthew</firstname>">
  <!ENTITY dhsurname   "<surname>Johnson</surname>">
  <!-- Please adjust the date whenever revising the manpage. -->
  <!ENTITY dhdate      "<date>April  28, 2007</date>">
  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1). -->
  <!ENTITY dhsection   "<manvolnum>1</manvolnum>">
  <!ENTITY dhemail     "<email>&lt;debian@matthew.ath.cx&gt;</email>">
  <!ENTITY dhusername  "Matthew Johnson">
  <!ENTITY dhucpackage "<refentrytitle>PESCETTI</refentrytitle>">
  <!ENTITY dhpackage   "pescetti">

  <!ENTITY debian      "<productname>Debian</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
  <!ENTITY gpl         "&gnu; <acronym>GPL</acronym>">
]>

<refentry>
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    <author>
      &dhfirstname;
      &dhsurname;
    </author>
    <copyright>
      <year>2007</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname>

    <refpurpose>Pseudo-Duplimate Generator</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>
    </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1>
    <title>DESCRIPTION</title>

    <para>This manual page documents briefly the
      <command>&dhpackage;</command> command.</para>

    <para>
   </para>
  </refsect1>
  <refsect1>
    <title>OPTIONS</title>

    <para>
      Here are a list of the available options and what they do. You must specify exactly one from
      --demo, --generate or --load.
    </para>

    <variablelist>
      <varlistentry>
        <term>--help</term>
        <listitem>
          <para>Prints the help text</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>--demo</term>
        <listitem>
          <para>Demonstration mode. Generates one hand with permutations and the tutorial for how to use them.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>--generate=N</term>
        <listitem>
          <para>Generate N random boards</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>--load=boards.txt</term>
        <listitem>
          <para>Load boards+analysis from boards.txt</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>--load-dds=boards.dds</term>
        <listitem>
          <para>Load boards from boards.dds in dds format</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>--load-analysis=tricks.txt</term>
        <listitem>
          <para>Load analysis from tricks.txt</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>--permutations=permutations.txt</term>
        <listitem>
          <para>Generate the permutations and save them to the given file</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>--curtains=curtains.txt</term>
        <listitem>
          <para>Save curtain cards to file curtains.txt</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>--save=boards.txt</term>
        <listitem>
          <para>Save the boards+analysis to boards.txt</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>--save-dds=boards.dds</term>
        <listitem>
          <para>Save the boards to boards.dds in dds format</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>--save-analysis=tricks.txt</term>
        <listitem>
          <para>Save the analysis to tricks.txt</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>--format=html|txt|pdf</term>
        <listitem>
          <para>Set the output mode to the given format</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>--title=title</term>
        <listitem>
          <para>Set the title for the output</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>--output=hands.txt</term>
        <listitem>
          <para>Print the hands to hands.txt, rather than to standard output</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>--stats</term>
        <listitem>
          <para>Generate statistics about the set of boards; included in the hands output</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>--analyze</term>
        <listitem>
          <para>Run the dds analyzer on the boards and print the resulting numberof tricks (warning SLOW)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>--criteria="criteria..."</term>
        <listitem>
          <para>A list of criteria to apply to each generated hand to generate specific hand types.
			 The list should be space separated and each item may be suffixed with a colon and a (fractional)
			 probability value which can be used to weight the criteria.
			 </para>
			 <para>
			 E.g. --criteria="weaknt:0.8 strongnt:0.5"
			 </para>
			 <para>
          Valid criteria are: unbalanced weaknt strongnt twont strongtwo weaktwo three twoclubs 4441 singlesuit twosuits partscore game slam game-invite slam-invite jumpshift jumpfit splinter bacon weird</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>--probability=factor</term>
        <listitem>
          <para>Generate hands matching the criteria with only the given
          probability. Factor is in the range 0 to 1. On each attempt to
          generate a board it is rejected if it doesn't match the criteria with
          the given probability.  A factor of about 0.8 gives roughly half
          matching boards</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsect1>
    <!--  -help -generate=N -load=<boards.txt> -permutations=<prefix> -demo -stats -analyze -save=<boards.txt> -format=<format> -title=<title> -output=<hands.txt>");
          System.out.println("   Formats: txt html pdf");
          -->
  <refsect1>
    <title>AUTHOR</title>

    <para>This manual page was written by &dhusername; &dhemail;. Permission is
      granted to copy, distribute and/or modify this document under
      the terms of the &gnu; General Public License, Version 2 as
      published by the Free Software Foundation.
    </para>
	<para>
	  On Debian systems, the complete text of the GNU General Public
	  License can be found in /usr/share/common-licenses/GPL.
	</para>

  </refsect1>
</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->


